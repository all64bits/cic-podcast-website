Subscribe:
`RSS Feed </rss/podcast.xml>`__,
`Spotify <https://open.spotify.com/show/3HdsdjtutzKq7JrXYUlXyp>`__,
`Google Podcasts <https://podcasts.google.com/feed/aHR0cHM6Ly9jb252ZXJzYXRpb25zaW5jb2RlLnh5ei9yc3MvcG9kY2FzdC54bWw>`__,
`iTunes <https://podcasts.apple.com/au/podcast/conversations-in-code/id1529818858>`__.

----

##############
Latest Episode
##############

.. always keep latest episode here

.. include:: 013.rst

----

********
Episodes
********

.. toctree::
   :maxdepth: 1

   013.rst
   012.rst
   011.rst
   010.rst
   009.rst
   008.rst
   007.rst
   006.rst
   005.rst
   004.rst
   003.rst
   002.rst
   001.rst


***********
Other Pages
***********

.. toctree::
   :maxdepth: 1

   about_us.rst
