
import sys, os

sys.path.insert(0, os.path.abspath(os.path.join('..', 'exts')))

extensions = [
    # support 'episode' role.
    "sphinx.ext.extlinks",

    # our own
    'audio',
    ]

extlinks = {
    'episode': ('https://conversationsincode.xyz/episodes/%s', 'episode '),
    }

# The suffix of source filenames.
source_suffix = '.rst'

exclude_patterns = ["template.rst"]
master_doc = 'index'

# General information about the project.
project = '' # XXX, leave empty so it doesn't show under logo text.

copyright = 'Creative Commons'

# without this it calls it 'documentation', which it's not
html_title = "Conversations in Code"
html_short_title = "Conversations in Code"

# visual noise for my purpose
html_show_copyright = False
html_show_sphinx = False
html_show_sourcelink = False

html_favicon = "../theme/misc/favicon.ico"

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = [
    "../theme",
    "../templates/static",
    ]

def setup(app):
    app.add_css_file("css/theme_overrides.css")

html_theme = 'alabaster'

templates_path = ["../templates"]

html_theme = 'alabaster'
html_theme_options = {
    "show_powered_by": False,
}
import alabaster
print(alabaster.__file__)
html_theme_path = [alabaster.get_path()]


# for our own :audio: role!
audio_base = "https://conversationsincode.xyz/episodes"
