

***************************************
Episode 11: Todo & Task List Management
***************************************

.. RSS BEGIN

In this episode we talk about our approach to managing TODO's and managing tasks.

As always, feedback is welcome.

----

Links:
   - `todo.txt <http://todotxt.org>`__
   - `Org Mode App (orgzly) <https://github.com/orgzly/orgzly-android>`__
   - `Todoist (proprietary task management) <https://todoist.com>`__
   - `Taskwarrior (command line task management) <https://taskwarrior.org/>`__
   - `uTimeClock (time tracking for Emacs) <https://gitlab.com/ideasman42/emacs-utimeclock>`__

.. RSS END

.. admonition:: Listen
   :class: episode

   Discussion about task management starts at: 12:20.

   .. audio:: cic_011_task_management

   :Length: 56:57
   :Released: 2021/05/12

   :Download:
      :episode:`MP3 <cic_011_task_management.mp3>`
..
   :Keywords: todo, task management
