
********
About us
********

Just two mates having an extended conversation about geeky tools, methods and goals.

|

**Mike Williams**

Mike works in project delivery for a utility company in Victoria, Australia.

Twitter: `@palooka_jock <https://twitter.com/palooka_jock>`__

|

**Campbell Barton**

Campbell is a developer with the Blender Foundation.

Hosts
=====

.. figure:: /images/mugshot_mike_and_campbell.jpg

   Campbell Barton (left), Mike Williams (right)
