# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

'''
Utility functions to help generating an RSS from plain Python data.
'''

from __future__ import annotations

__all__ = (
    'file_size_in_bytes',
    'xml_from_dict',
    'rst_extract_title'
    'rst_extract_field_text'
    'rst_extract_text_by_tagname_iter',
)


from typing import (
    Tuple,
    Sequence,
    Dict,
    List,
    Union,
    Any,
)

XMLFromDict = Tuple[str, Dict[str, str], Union[bytes, Sequence[Any]]]

def xml_from_dict(
        data_root: XMLFromDict,
        level: int,
) -> str:
    indent = '  '
    from xml.sax.saxutils import (
        quoteattr,
        escape,
    )
    this_indent = indent * level
    result = []
    for key, attr, data in data_root:
        if data == []:
            if attr == {}:
                result.append(f'{this_indent:s}<{key:s} />\n')
            else:
                result.append(
                    '{:s}<{:s} {:s} />\n'.format(
                        this_indent,
                        key,
                        ' '.join(
                            f'{attr_key:s}={quoteattr(attr_value):s}'
                            for (attr_key, attr_value) in attr.items()
                        ),
                    ),
                )
        else:
            is_nested = type(data) in (list, tuple)
            if attr == {}:
                result.append(f'{indent * level:s}<{key:s}>')
                if is_nested:
                    result.append('\n')
            else:
                result.append(
                    '{:s}<{:s} {:s}>\n'.format(
                        this_indent,
                        key,
                        ' '.join(
                            f'{attr_key:s}={quoteattr(attr_value):s}'
                            for (attr_key, attr_value) in attr.items()
                        ),
                    ),
                )
            if is_nested:
                result.append(xml_from_dict(data, level + 1))
                result.append(this_indent)
            else:
                # FIXME, have a way not to use bytes for this.
                if type(data) is bytes:
                    result.append('<![CDATA[' + data.decode('utf-8') + ']]>')
                else:
                    result.append(escape(str(data)))

            result.append(f'</{key:s}>\n')

    return ''.join(result)


def file_size_in_bytes(filepath) -> int:
    import os
    with open(filepath, 'rb') as fh:
        fh.seek(0, os.SEEK_END)
        return fh.tell()


def smpte_from_seconds(seconds):
    seconds = int(seconds)
    hours = seconds // 3600
    mins = (seconds // 60) % 60
    seconds = seconds % 60
    if hours:
        return '%02d:%02d:%02d' % (hours, mins, seconds)
    if mins:
        return '%02d:%02d' % (mins, seconds)
    return '%02d' % seconds


def audio_file_info_from_ffmpeg(filepath):
    # Use FFMPEG's FFPROBE.
    import subprocess
    import json
    data = subprocess.check_output([
        'ffprobe',
        '-v', 'quiet',
        '-print_format', 'json=compact=1',
        '-show_format',
        filepath,
    ], stderr=subprocess.DEVNULL)
    return json.loads(data.decode('utf-8'))


def audio_file_tag(filepath, *, track_number, title, artist, year):
    import subprocess
    subprocess.check_output([
        'id3v2',
        '--delete-all',
        filepath,
    ])
    subprocess.check_output([
        'id3v2',
        '--song', title,
        '--artist', artist,
        '--track', str(track_number),
        '--year', str(year),
        filepath,
    ])


# -----------------------------------------------------------------------------
# Extract From RST

# Run once to setup customization to ignore Sphinx & custom directives.
def _rst_setup() -> None:
    import docutils
    from docutils.parsers.rst import directives, roles
    from docutils.parsers.rst.languages.en import roles as roles_dict

    def directive_ignore(
            name, arguments, options, content, lineno,
            content_offset, block_text, state, state_machine,
    ):
        '''
        Used to explicitly mark as doctest blocks things that otherwise
        wouldn't look like doctest blocks.

        Note this doesn't ignore child nodes.
        '''
        text = '\n'.join(content)
        '''
        if re.match(r'.*\n\s*\n', block_text):
            warning('doctest-ignore on line %d will not be ignored, '
                 'because there is\na blank line between ".. doctest-ignore::"'
                 ' and the doctest example.' % lineno)
        '''
        return [docutils.nodes.doctest_block(text, text, codeblock=True)]

    def directive_ignore_recursive(
            name, arguments, options, content, lineno,
            content_offset, block_text, state, state_machine,
    ):
        '''
        Ignore everything under this directive (use with care!)
        '''
        return []

    directive_ignore_recursive.content = True

    # ones we want to check
    directives.register_directive('index', directive_ignore)

    # Recursive ignore, take care!
    directives.register_directive('audio', directive_ignore_recursive)
    # directives.register_directive('admonition', directive_ignore_recursive)

    # Dummy roles

    class RoleIgnore(docutils.nodes.Inline, docutils.nodes.TextElement):
        def visit_RoleIgnore(self) -> None:
            pass
        def depart_RoleIgnore(self) -> None:
            pass

    def role_ignore(
            name, rawtext, text, lineno, inliner,
            options={}, content=[],
    ):
        # Recursively parse the contents of the index term, in case it
        # contains a substitution (like |alpha|).
        nodes, msgs = inliner.parse(text, lineno, memo=inliner, parent=inliner.parent)
        # 'text' instead of 'rawtext' because it doesn't contain the :role:
        return [RoleIgnore(text, '', *nodes, **options)], []

    class RoleIgnoreRecursive(docutils.nodes.Inline, docutils.nodes.TextElement):
        pass

    def role_ignore_recursive(
            name, rawtext, text, lineno, inliner,
            options={}, content=[],
    ):
        return [RoleIgnore('', '', *(), **{})], []

    roles.register_canonical_role('episode', role_ignore)
    # Quiet warning.
    roles_dict['episode'] = 'episode'

    # roles.register_canonical_role('episode', role_ignore_recursive)


def _rst_to_doctree(filedata, filename):
    # filename only used as an ID
    import docutils.parsers.rst
    parser = docutils.parsers.rst.Parser()
    doc = docutils.utils.new_document(filename)
    doc.settings.tab_width = 3
    doc.settings.pep_references = False
    doc.settings.rfc_references = False
    doc.settings.syntax_highlight = False

    doc.settings.raw_enabled = True  # TODO, check how this works!
    doc.settings.file_insertion_enabled = True
    doc.settings.character_level_inline_markup = False  # TODO, whats sphinx do?
    doc.settings.trim_footnote_reference_space = False  # doesn't seem important

    parser.parse(filedata, doc)
    # import IPython
    # IPython.embed()
    return doc


# -----------------------------------------------------------------------------
# Public RST API


def rst_parse_file(rst_file):
    with open(rst_file, 'r', encoding='utf-8') as f:
        return _rst_to_doctree(f.read(), rst_file)


def rst_extract_title(doc):
    def first_of_tag(nodes, tagname):
        for n in nodes:
            if n.tagname == tagname:
                return n.astext()
            result = first_of_tag(n.children, tagname)
            if result:
                return result

    return first_of_tag(doc, 'title')


def _rst_flatten_nodes(node, tagname, level):
    # print(node.tagname)
    if node.tagname == tagname:
        yield (level, node)
        return
    if node.children:
        for node_child in node.children:
            yield from _rst_flatten_nodes(node_child, tagname, level + 1)
    else:
        yield (level, node)


def rst_extract_first_paragraph(doc):
    has_paragraph = False
    paragraph_level = -1
    paragraph_list = []
    for level, node in _rst_flatten_nodes(doc, 'paragraph', 0):
        is_paragraph = node.tagname == 'paragraph'
        if is_paragraph:
            if not has_paragraph:
                paragraph_level = level
                has_paragraph = True
        if has_paragraph:
            if not is_paragraph or level != paragraph_level:
                break
            paragraph_list.append(node)

    description = ' '.join([node.astext().replace('\n', ' ') for node in paragraph_list])
    return description


def rst_extract_field_text(doc, field):
    for _level, node in _rst_flatten_nodes(doc, 'field', 0):
        if node.tagname == 'field':
            if node.children[0].astext() == field:
                return node.children[1].astext()


def rst_extract_field_value(doc, field):
    for _level, node in _rst_flatten_nodes(doc, 'field', 0):
        if node.tagname == 'field':
            if node.children[0].astext() == field:
                return node.children[1]


def rst_extract_text_by_tagname_iter(doc, tagname):
    for _level, node in _rst_flatten_nodes(doc, tagname, 0):
        if node.tagname == tagname:
            yield node.astext()


# -----------------------------------------------------------------------------
# Public RST As HTML API

def rst_as_html_string(rst_data):

    import docutils
    import docutils.core
    from docutils.writers.html4css1 import HTMLTranslator
    HTMLTranslator.visit_RoleIgnore = lambda _a, _b: None
    HTMLTranslator.depart_RoleIgnore = lambda _a, _b: None

    from docutils.writers.html4css1 import Writer,HTMLTranslator
    class HTMLFragmentTranslator( HTMLTranslator ):
        def __init__(self, document) -> None:
            HTMLTranslator.__init__( self, document )
            self.head_prefix = ['', '', '', '', '']
            self.head = []
            self.body_prefix = []
            self.body_suffix = []
            self.stylesheet = []
        def astext(self):
            return ''.join(self.body)

    html_fragment_writer = Writer()
    html_fragment_writer.translator_class = HTMLFragmentTranslator

    args = {'input_encoding': 'unicode', 'output_encoding': 'unicode'}
    return docutils.core.publish_string(source=rst_data, writer=html_fragment_writer, settings_overrides=args)


# -----------------------------------------------------------------------------
# Internal one off setup

_rst_setup()
