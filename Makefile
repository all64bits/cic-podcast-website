
SPHINXOPTS    =
PAPER         =
SPHINXBUILD   = sphinx-build
BUILDDIR      = build
RSSDIR        = rss


# os specific
ifeq ($(OS), Darwin)
	# OSX
	OPEN_CMD="open"
else
	# Linux/FreeBSD
	OPEN_CMD="xdg-open"
endif

all: FORCE
	@echo ""
	@echo "Static Site"
	@echo "==========="
	$(SPHINXBUILD) -b html $(SPHINXOPTS) ./content "$(BUILDDIR)/html"
	cp -R $(RSSDIR) $(BUILDDIR)/html/
	@echo "To view, run:"
	@echo "  " $(OPEN_CMD) " \"$(BUILDDIR)/html/index.html\""

clean: FORCE
	@echo ""
	@echo "Clean"
	@echo "====="
	rm -rf "$(BUILDDIR)/html" "$(BUILDDIR)/latex"

# TODO, use real location
upload: all FORCE
	@echo ""
	@echo "Upload"
	@echo "======"
	rsync --progress --chmod=777 --chown $(USER):podcasters -rlpgoDve "ssh -p 22" $(BUILDDIR)/html/* $(USER)@conversationsincode.xyz:/var/www/html

rss: FORCE
	@echo ""
	@echo "RSS"
	@echo "==="
	./rss_generate/rss_from_rst --output rss/podcast.xml

upload_audio: FORCE
	@echo ""
	@echo "Upload Audio"
	@echo "============"
	rsync -avp episodes/*.mp3 $(USER)@conversationsincode.xyz:/var/www/html/episodes/

world: all rss upload upload_audio

FORCE:
