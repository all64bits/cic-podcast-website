
Conversations in Code
=====================

Website Source
--------------

This is the source files for the `Conversations in Code <https://conversationsincode.xyz>`__ website, forked from the codebase for the Blender Podcast.

It may be of interest to anyone looking to setup a podcast using static generated HTML.

- Uses Sphinx to generate the website.
- A simple extension to embed audio. (see: ``exts/audio.py``),
  written for this site.
- Minor ``css`` edits to remove Python specific text.
