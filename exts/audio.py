#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
from docutils import nodes
from docutils.parsers.rst import (
    directives,
    Directive,
)


def get_size(d, key):
    if key not in d:
        return None
    m = re.match("(\d+)(|%|px)$", d[key])
    if not m:
        raise ValueError("invalid size %r" % d[key])
    return int(m.group(1)), m.group(2) or "px"


def css(d):
    return "; ".join(sorted("%s: %s" % kv for kv in d.items()))


class audio(nodes.General, nodes.Element):
    pass


def visit_audio_node(self, node):

    audio_base = self.builder.app.config.audio_base

    # CSS
    width = node["width"]

    # build audio tag...
    audio_tag = ["audio"]

    for bool_opt in [
            "autoplay",
            "controls",
            "loop",
            "muted",
            "preload",
    ]:

        if node[bool_opt]:
            audio_tag.append(bool_opt)

    style = {
        "width": ("%d%s" % width) if width else "100%",
    }

    attrs = {
        "style": css(style),
    }

    file_id = node["id"]

    self.body.append(self.starttag(node, " ".join(audio_tag), **attrs))
    self.body.append('<source src="%s/%s.ogg" type="audio/ogg">' % (audio_base, file_id))
    self.body.append('<source src="%s/%s.mp3" type="audio/mp3">' % (audio_base, file_id))
    self.body.append("</audio>")


def depart_audio_node(self, node):
    pass


class Audio(Directive):
    has_content = True
    required_arguments = 1
    optional_arguments = 0
    final_argument_whitespace = False
    option_spec = {
        "width": directives.unchanged,
        "autoplay": directives.unchanged,
        "controls": directives.unchanged,
        "loop": directives.unchanged,
        "muted": directives.unchanged,
        "preload": directives.unchanged,
    }

    def run(self):
        width = get_size(self.options, "width")

        # could add more
        controls = int(self.options.get("controls", "1"))
        autoplay = int(self.options.get("autoplay", "0"))
        loop = int(self.options.get("loop", "0"))
        muted = int(self.options.get("muted", "0"))
        preload = int(self.options.get("preload", "0"))

        return [
            audio(
                id=self.arguments[0],
                width=width,
                autoplay=autoplay,
                controls=controls,
                loop=loop,
                muted=muted,
                preload=preload,
            ),
        ]


def setup(app):
    app.add_node(audio, html=(visit_audio_node, depart_audio_node))
    app.add_directive("audio", Audio)

    app.add_config_value('audio_base', None, 'env')
